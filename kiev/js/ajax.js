$(document).ready(function() {
    $('form').submit(function () {
        var formID = $(this).attr('id'); // Получение ID формы
        var formNm = $('#' + formID);
        $.ajax({
            type: 'POST',
            url: 'mail.php', // Обработчик формы отправки
            data: formNm.serialize()
        }).done(function() {
            $(formNm).find("input", "textarea").val("");
            alert("Thank you for your invoice! We will contact you soon.");
            $('form').trigger("reset");
        });
        return false;
    });
});