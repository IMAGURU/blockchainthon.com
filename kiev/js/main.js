$(document).ready(function() {
// ==================================================
// smooth scrolling to anchors full &mobile format
// ==================================================
    if ($(window).width() <= '768'){
        $('#main-nav-container').ddscrollSpy({ // initialize first demo
            scrolltopoffset: 0
        });
        $('#footer-nav-container').ddscrollSpy({ // initialize first demo
            scrolltopoffset: 0
        });
    } else {
        $('#main-nav-container').ddscrollSpy({ // initialize first demo
            scrolltopoffset: -58
        });
        $('#footer-nav-container').ddscrollSpy({ // initialize first demo
            scrolltopoffset: -58
        });
    }
// =======================================
// slider
// =======================================
    $('.blockchain-slider').slick({
        arrows: false,
        dots: true
    });
// =======================================
// To top button
// =======================================
    var delay = 1000; // Задержка прокрутки
    $('#top').click(function () { // При клике по кнопке "Наверх" попадаем в эту функцию
        /* Плавная прокрутка наверх */
        $('body, html').animate({
            scrollTop: 0
        }, delay);
    });
// =======================================
// Map
// =======================================
    $('span.london').mouseover(function () {
        $('path.london').addClass('active');
        $('path.cap-london').addClass('active')
    });
    $('span.london').mouseout(function () {
        $('path.london').removeClass('active');
        $('path.cap-london').removeClass('active')
    });

    $('span.madrid').mouseover(function () {
        $('path.madrid').addClass('active');
        $('path.cap-madrid').addClass('active')
    });
    $('span.madrid').mouseout(function () {
        $('path.madrid').removeClass('active');
        $('path.cap-madrid').removeClass('active')
    });

    $('span.minsk').mouseover(function () {
        $('polygon.minsk').addClass('active');
        $('path.cap-minsk').addClass('active')
    });
    $('span.minsk').mouseout(function () {
        $('polygon.minsk').removeClass('active');
        $('path.cap-minsk').removeClass('active')
    });

    $('span.tokyo').mouseover(function () {
        $('path.tokyo').addClass('active');
        $('path.cap-tokyo').addClass('active')
    });
    $('span.tokyo').mouseout(function () {
        $('path.tokyo').removeClass('active');
        $('path.cap-tokyo').removeClass('active')
    });
    $('span.singapore').mouseover(function () {
        $('path.singapore').addClass('active');
        $('path.cap-singapore').addClass('active')
    });
    $('span.singapore').mouseout(function () {
        $('path.singapore').removeClass('active');
        $('path.cap-singapore').removeClass('active')
    });

    $('span.lumpur').mouseover(function () {
        $('path.lumpur').addClass('active');
        $('path.cap-lumpur').addClass('active')
    });
    $('span.lumpur').mouseout(function () {
        $('path.lumpur').removeClass('active');
        $('path.cap-lumpur').removeClass('active')
    });

    $('span.kong').mouseover(function () {
        $('path.kong').addClass('active');
        $('path.cap-kong').addClass('active')
    });
    $('span.kong').mouseout(function () {
        $('path.kong').removeClass('active');
        $('path.cap-kong').removeClass('active')
    });

    $('span.seul').mouseover(function () {
        $('path.seul').addClass('active');
        $('path.cap-seul').addClass('active')
    });
    $('span.seul').mouseout(function () {
        $('path.seul').removeClass('active');
        $('path.cap-seul').removeClass('active')
    });
});