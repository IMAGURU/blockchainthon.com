$(document).ready(function() {
// ==================================================
// smooth scrolling to anchors full &mobile format
// ==================================================
    if ($(window).width() <= '768'){
        $('#main-nav-container').ddscrollSpy({ // initialize first demo
            scrolltopoffset: 0
        });
        $('#footer-nav-container').ddscrollSpy({ // initialize first demo
            scrolltopoffset: 0
        });
    } else {
        $('#main-nav-container').ddscrollSpy({ // initialize first demo
            scrolltopoffset: -58
        });
        $('#footer-nav-container').ddscrollSpy({ // initialize first demo
            scrolltopoffset: -58
        });
    }
// =======================================
// smooth scrolling to anchors
// =======================================
    $(".participate").on("click","a", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор с атрибута href
        var id  = $(this).attr('href'),

            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top - 100;

        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({scrollTop: top}, 1000);
    });
// =======================================
// clipboard
// =======================================
    var btn = document.getElementById('btn');
    var clipboard = new ClipboardJS(btn);
    clipboard.on('success', function(e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);

        e.clearSelection();
    });
    clipboard.on('error', function(e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });

    var btnqr = document.getElementById('btnqr');
    var clipboard2 = new ClipboardJS(btnqr);
    clipboard2.on('success', function(e) {
        console.log(e);
    });
    clipboard2.on('error', function(e) {
        console.log(e);
    });
// =======================================
// slider
// =======================================
    $('.blockchain-slider').slick({
        arrows: false,
        dots: true
    });

// $(document).ready(function newSlider(){
//     if ($(window).width() <= '425'){
//         if (! $('#mentors .process-item-container').hasClass('mentors-slider')){
//             $('#mentors .process-item-container').addClass('mentors-slider');
//             $('.mentors-slider').slick({
//                 arrows: false,
//                 dots: true
//             });
//         }
//     } else {
//         if ( $('#mentors .process-item-container').hasClass('slick-slider')){
//             $('#mentors .process-item-container').removeClass('mentors-slider slick-slider')
//         }
//     }
//     $(window).resize(newSlider);
// });
// =======================================
// To top button
// =======================================
    var delay = 1000; // Задержка прокрутки
    $('#top').click(function () { // При клике по кнопке "Наверх" попадаем в эту функцию
        /* Плавная прокрутка наверх */
        $('body, html').animate({
            scrollTop: 0
        }, delay);
    });
// =======================================
// One height of tabs in Schedule
// =======================================
//     var hTabs = $('.schedule-wrapper');
//     if(hTabs) {
//         var ht = Math.max.apply(Math, $('.schedule .tab-content-item').map(function(){
//             return $(this).height();
//         }).get());
//         hTabs.height(ht + 180);
//     }
// =======================================
// Map
// =======================================
    $('span.london').mouseover(function () {
        $('path.london').addClass('active');
        $('path.cap-london').addClass('active')
    });
    $('span.london').mouseout(function () {
        $('path.london').removeClass('active');
        $('path.cap-london').removeClass('active')
    });

    $('span.madrid').mouseover(function () {
        $('path.madrid').addClass('active');
        $('path.cap-madrid').addClass('active')
    });
    $('span.madrid').mouseout(function () {
        $('path.madrid').removeClass('active');
        $('path.cap-madrid').removeClass('active')
    });

    $('span.minsk').mouseover(function () {
        $('polygon.minsk').addClass('active');
        $('path.cap-minsk').addClass('active')
    });
    $('span.minsk').mouseout(function () {
        $('polygon.minsk').removeClass('active');
        $('path.cap-minsk').removeClass('active')
    });

    $('span.tokyo').mouseover(function () {
        $('path.tokyo').addClass('active');
        $('path.cap-tokyo').addClass('active')
    });
    $('span.tokyo').mouseout(function () {
        $('path.tokyo').removeClass('active');
        $('path.cap-tokyo').removeClass('active')
    });

    $('span.singapore').mouseover(function () {
        $('path.singapore').addClass('active');
        $('path.cap-singapore').addClass('active')
    });
    $('span.singapore').mouseout(function () {
        $('path.singapore').removeClass('active');
        $('path.cap-singapore').removeClass('active')
    });

    $('span.lumpur').mouseover(function () {
        $('path.lumpur').addClass('active');
        $('path.cap-lumpur').addClass('active')
    });
    $('span.lumpur').mouseout(function () {
        $('path.lumpur').removeClass('active');
        $('path.cap-lumpur').removeClass('active')
    });

    $('span.kong').mouseover(function () {
        $('path.kong').addClass('active');
        $('path.cap-kong').addClass('active')
    });
    $('span.kong').mouseout(function () {
        $('path.kong').removeClass('active');
        $('path.cap-kong').removeClass('active')
    });

    $('span.seoul').mouseover(function () {
        $('path.seul').addClass('active');
        $('path.cap-seul').addClass('active')
    });
    $('span.seoul').mouseout(function () {
        $('path.seul').removeClass('active');
        $('path.cap-seul').removeClass('active')
    });

    $('span.kiev').mouseover(function () {
        $('polygon.kiev').addClass('active');
        $('path.cap-kiev').addClass('active')
    });
    $('span.kiev').mouseout(function () {
        $('polygon.kiev').removeClass('active');
        $('path.cap-kiev').removeClass('active')
    });

});